package com.corporation.musicapptest.data.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




 class Datum{
    @SerializedName("id")
    @Expose
    private var id: Int? = null
    @SerializedName("readable")
    @Expose
    private var readable: Boolean? = null
    @SerializedName("title")
    @Expose
    private var title: String? = null
    @SerializedName("title_short")
    @Expose
    private var titleShort: String? = null
    @SerializedName("title_version")
    @Expose
    private var titleVersion: String? = null
    @SerializedName("link")
    @Expose
    private var link: String? = null
    @SerializedName("duration")
    @Expose
    private var duration: Int? = null
    @SerializedName("rank")
    @Expose
    private var rank: Int? = null
    @SerializedName("explicit_lyrics")
    @Expose
    private var explicitLyrics: Boolean? = null
    @SerializedName("explicit_content_lyrics")
    @Expose
    private var explicitContentLyrics: Int? = null
    @SerializedName("explicit_content_cover")
    @Expose
    private var explicitContentCover: Int? = null
    @SerializedName("preview")
    @Expose
    private var preview: String? = null
    @SerializedName("artist")
    @Expose
    private var artist: Artist? = null
    @SerializedName("album")
    @Expose
    private var album: Album? = null
    @SerializedName("type")
    @Expose
    private var type: String? = null

    fun getId(): Int? {
       return id
    }

    fun setId(id: Int?) {
       this.id = id
    }

    fun getReadable(): Boolean? {
       return readable
    }

    fun setReadable(readable: Boolean?) {
       this.readable = readable
    }

    fun getTitle(): String? {
       return title
    }

    fun setTitle(title: String?) {
       this.title = title
    }

    fun getTitleShort(): String? {
       return titleShort
    }

    fun setTitleShort(titleShort: String?) {
       this.titleShort = titleShort
    }

    fun getTitleVersion(): String? {
       return titleVersion
    }

    fun setTitleVersion(titleVersion: String?) {
       this.titleVersion = titleVersion
    }

    fun getLink(): String? {
       return link
    }

    fun setLink(link: String?) {
       this.link = link
    }

    fun getDuration(): Int? {
       return duration
    }

    fun setDuration(duration: Int?) {
       this.duration = duration
    }

    fun getRank(): Int? {
       return rank
    }

    fun setRank(rank: Int?) {
       this.rank = rank
    }

    fun getExplicitLyrics(): Boolean? {
       return explicitLyrics
    }

    fun setExplicitLyrics(explicitLyrics: Boolean?) {
       this.explicitLyrics = explicitLyrics
    }

    fun getExplicitContentLyrics(): Int? {
       return explicitContentLyrics
    }

    fun setExplicitContentLyrics(explicitContentLyrics: Int?) {
       this.explicitContentLyrics = explicitContentLyrics
    }

    fun getExplicitContentCover(): Int? {
       return explicitContentCover
    }

    fun setExplicitContentCover(explicitContentCover: Int?) {
       this.explicitContentCover = explicitContentCover
    }

    fun getPreview(): String? {
       return preview
    }

    fun setPreview(preview: String?) {
       this.preview = preview
    }

    fun getArtist(): Artist? {
       return artist
    }

    fun setArtist(artist: Artist?) {
       this.artist = artist
    }

    fun getAlbum(): Album? {
       return album
    }

    fun setAlbum(album: Album?) {
       this.album = album
    }

    fun getType(): String? {
       return type
    }

    fun setType(type: String?) {
       this.type = type
    }

 }