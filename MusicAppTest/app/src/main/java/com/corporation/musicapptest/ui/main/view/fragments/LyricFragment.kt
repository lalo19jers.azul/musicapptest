package com.corporation.musicapptest.ui.main.view.fragments

import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.corporation.musicapptest.R
import com.corporation.musicapptest.data.api.ApiService
import com.corporation.musicapptest.data.api.MusicHelper
import com.corporation.musicapptest.data.model.Lyric
import kotlinx.android.synthetic.main.fragment_lyric.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * A simple [Fragment] subclass.
 */
class LyricFragment : Fragment() {


    lateinit var artist: String
    lateinit var songName: String
    lateinit var root : View
    lateinit var musicHelper: MusicHelper
    lateinit var apiService: ApiService


    companion object {
        const val KEY_ARTIST = "KEY_ARTIST"
        const val KEY_SONG = "KEY_SONG"

        fun newInstance(artist: String, titleSong: String): LyricFragment {
            val args = Bundle()
            args.putSerializable(KEY_ARTIST, artist)
            args.putSerializable(KEY_SONG, titleSong)
            val fragment = LyricFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_lyric, container, false)
        arguments?.let { artist = it.getSerializable(KEY_ARTIST) as String }
        arguments?.let { songName = it.getSerializable(KEY_SONG) as String }

        println("$artist ///// $songName")

        root.tvLyrics.movementMethod = ScrollingMovementMethod()

        retrofitInit()
        getLyric(artist, songName)
        return root
    }


    private fun retrofitInit() {
        musicHelper = MusicHelper.instance!!
        apiService = musicHelper.authApiService
    }

    private fun getLyric(artist: String, songName: String){

        val call: Call<Lyric> = apiService.getLyric(artist, songName)


        call.enqueue(object : Callback<Lyric> {
            override fun onFailure(call: Call<Lyric>, t: Throwable) {
                t.message
            }

            override fun onResponse(
                call: Call<Lyric>,
                response: Response<Lyric>
            ) {
                if (response.isSuccessful) {
                    response.body()
                    root.tvLyrics.text = response.body()!!.lyrics.toString()
                } else {
                    root.tvLyrics.text = getString(R.string.not_found)
                }
            }

        })
    }

}
