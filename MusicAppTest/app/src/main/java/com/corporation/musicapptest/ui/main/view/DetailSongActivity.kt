package com.corporation.musicapptest.ui.main.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.corporation.musicapptest.R

class DetailSongActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_song)
    }
}
