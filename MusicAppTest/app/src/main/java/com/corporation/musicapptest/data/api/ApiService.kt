package com.corporation.musicapptest.data.api

import com.corporation.musicapptest.data.model.Lyric
import com.corporation.musicapptest.data.model.SearchMusicResponse
import com.corporation.musicapptest.ui.main.view.DetailSongActivity
import retrofit2.Call
import retrofit2.http.*

interface  ApiService {
    @Headers("Content-Type:application/json")
    @GET("/suggest/{search_term}")
    suspend fun getSuggest(@Path("search_term") search_term: String) : SearchMusicResponse



    @GET("/suggest/{search_term}")
     fun doMethod(@Path("search_term") search_term: String): Call<SearchMusicResponse>

    @GET("/v1/{Artist}/{Song}")
    fun getLyric(@Path("Artist") artist: String, @Path("Song") song: String): Call<Lyric>
}