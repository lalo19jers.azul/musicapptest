package com.corporation.musicapptest.data.api

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Singleton class of retrofit client
 */
class MusicHelper {
    private val apiService: ApiService
    /**
     *
     */
    val authApiService: ApiService
        get() = apiService

    companion object {


        private  const val BASE_URL = "https://api.lyrics.ovh"
        private lateinit var retrofit: Retrofit
        /**
         * Singleton Method
         * @return
         */
        var instance: MusicHelper? = null
            get() {
                if (field == null) {
                    field = MusicHelper()
                }
                return field
            }
            private set
    }

    init {
        val gson = GsonBuilder()
            .setLenient()
            //.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create()
        val okHttpClientBuilder = OkHttpClient.Builder()
            .readTimeout(3, TimeUnit.MINUTES)
            .writeTimeout(3, TimeUnit.MINUTES)
            .connectTimeout(3, TimeUnit.MINUTES)
        // okHttpClientBuilder.addInterceptor(new AuthInterceptor());
        val client = okHttpClientBuilder.build()
        retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            //.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            // .addConverterFactory(GsonConverterFactory.create(gson))
            .client(client)
            .build()
        apiService =
            retrofit.create<ApiService>(
                ApiService::class.java)
    }
}