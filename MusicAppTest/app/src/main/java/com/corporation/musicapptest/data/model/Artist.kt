package com.corporation.musicapptest.data.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class Artist {
    @SerializedName("id")
    @Expose
    private var id: Int? = null
    @SerializedName("name")
    @Expose
    private var name: String? = null
    @SerializedName("link")
    @Expose
    private var link: String? = null
    @SerializedName("picture")
    @Expose
    private var picture: String? = null
    @SerializedName("picture_small")
    @Expose
    private var pictureSmall: String? = null
    @SerializedName("picture_medium")
    @Expose
    private var pictureMedium: String? = null
    @SerializedName("picture_big")
    @Expose
    private var pictureBig: String? = null
    @SerializedName("picture_xl")
    @Expose
    private var pictureXl: String? = null
    @SerializedName("tracklist")
    @Expose
    private var tracklist: String? = null
    @SerializedName("type")
    @Expose
    private var type: String? = null

    fun getId(): Int? {
        return id
    }

    fun setId(id: Int?) {
        this.id = id
    }

    fun getName(): String? {
        return name
    }

    fun setName(name: String?) {
        this.name = name
    }

    fun getLink(): String? {
        return link
    }

    fun setLink(link: String?) {
        this.link = link
    }

    fun getPicture(): String? {
        return picture
    }

    fun setPicture(picture: String?) {
        this.picture = picture
    }

    fun getPictureSmall(): String? {
        return pictureSmall
    }

    fun setPictureSmall(pictureSmall: String?) {
        this.pictureSmall = pictureSmall
    }

    fun getPictureMedium(): String? {
        return pictureMedium
    }

    fun setPictureMedium(pictureMedium: String?) {
        this.pictureMedium = pictureMedium
    }

    fun getPictureBig(): String? {
        return pictureBig
    }

    fun setPictureBig(pictureBig: String?) {
        this.pictureBig = pictureBig
    }

    fun getPictureXl(): String? {
        return pictureXl
    }

    fun setPictureXl(pictureXl: String?) {
        this.pictureXl = pictureXl
    }

    fun getTracklist(): String? {
        return tracklist
    }

    fun setTracklist(tracklist: String?) {
        this.tracklist = tracklist
    }

    fun getType(): String? {
        return type
    }

    fun setType(type: String?) {
        this.type = type
    }
}