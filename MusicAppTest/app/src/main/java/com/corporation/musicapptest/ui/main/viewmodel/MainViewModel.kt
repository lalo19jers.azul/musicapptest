package com.corporation.musicapptest.ui.main.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.corporation.musicapptest.data.repository.MainRepositoty
import com.corporation.musicapptest.utils.Resource
import kotlinx.coroutines.Dispatchers
import okhttp3.Dispatcher
import java.lang.Exception

class MainViewModel(private val mainRepositoty: MainRepositoty): ViewModel() {
    fun getSuggest() = liveData(Dispatchers.IO){
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = mainRepositoty.getSuggest()))
        }catch (ex: Exception){
            emit(Resource.error(data = null, message = ex.message ?: "Error Ocurred!"))
        }
    }
}