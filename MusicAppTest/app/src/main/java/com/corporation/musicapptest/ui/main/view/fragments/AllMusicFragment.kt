package com.corporation.musicapptest.ui.main.view.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager

import com.corporation.musicapptest.R
import com.corporation.musicapptest.data.api.ApiService
import com.corporation.musicapptest.data.api.MusicHelper
import com.corporation.musicapptest.data.model.Datum
import com.corporation.musicapptest.data.model.SearchMusicResponse
import com.corporation.musicapptest.ui.main.adapter.MainAdapter
import com.corporation.musicapptest.ui.main.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.fragment_all_music.*
import kotlinx.android.synthetic.main.fragment_all_music.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * A simple [Fragment] subclass.
 */
class AllMusicFragment : Fragment() {

    private lateinit var viewModel: MainViewModel



    lateinit var root: View
    lateinit var musicHelper: MusicHelper
    lateinit var apiService: ApiService
    private lateinit var adapter: MainAdapter

    override fun onResume() {
        super.onResume()
        Watcher()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_all_music, container, false)
        retrofitInit()
        setupUI()
        lateinit var musicHelper: MusicHelper
        lateinit var apiService: ApiService

        return root
    }


    private fun retrofitInit() {
        musicHelper = MusicHelper.instance!!
        apiService = musicHelper.authApiService
    }

    private fun Watcher() {
        root.etSearchTerm.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                //setupViewModel(p0.toString())
                query(p0.toString())
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })

    }


    private fun query(searchTerm: String) {


        val call: Call<SearchMusicResponse> = apiService.doMethod(searchTerm)


        call.enqueue(object : Callback<SearchMusicResponse> {
            override fun onFailure(call: Call<SearchMusicResponse>, t: Throwable) {
                t.message
            }

            override fun onResponse(
                call: Call<SearchMusicResponse>,
                response: Response<SearchMusicResponse>
            ) {
                if (response.isSuccessful) {
                    response.body()

                    retrieveList(response.body()!!.getData() as List<Datum>)
                } else {
                    response.errorBody()
                }
            }

        })
    }

    /* private fun setupViewModel(searchTerm: String){
        //  viewModel = ViewModelProviders.of(this, ViewModelFactory(ApiHelper(RetrofitBuilder.apiService))).get(MainViewModel::class.java)
          //viewModel = ViewModelProviders.of(this, ViewModelFactory(ApiHelper(RetrofitBuilder.apiService, searchTerm))).get(MainViewModel::class.java)

         viewModel = ViewModelProviders.of(this, ViewModelFactory(ApiHelper(apiService, searchTerm))).get(MainViewModel::class.java)
          setupObservers()
      }*/

    /*  private fun setupObservers(){
      viewModel.getSuggest().observe(this, Observer {
          it?.let { resource ->
              when (resource.status){
                  SUCCESS -> {
                      rvMusic.visibility = View.VISIBLE
                      pbMusic.visibility = View.GONE
                      resource.data?.let { suggest -> retrieveList(suggest as List<Datum>) }
                  }
                  ERROR -> {
                      rvMusic.visibility = View.GONE
                      pbMusic.visibility = View.GONE
                      ivNotFound.visibility = View.VISIBLE
                      Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                      Log.d("ERROR", it.message)
                      println(it.message)


                  }
                  LOADING -> {
                      pbMusic.visibility = View.VISIBLE
                      rvMusic.visibility = View.GONE
                      ivNotFound.visibility = View.GONE
                  }
              }
          }
      })
  }*/


    private fun retrieveList(suggest: List<Datum>){
        var musicAdapter = MainAdapter(suggest)
        rvMusic.adapter = musicAdapter
        musicAdapter.setItemClickListener(object : MainAdapter.ItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                val newFragment = LyricFragment.newInstance(suggest.get(position).getArtist()!!.getName().toString(),
                    suggest.get(position).getTitleShort().toString())
                val transaction = fragmentManager!!.beginTransaction()
                transaction.replace(R.id.frag_container, newFragment)
                transaction.addToBackStack(null)
                transaction.commit()
            }
        })


    }
    private fun setupUI() {
        root.rvMusic.layoutManager = LinearLayoutManager(root.context)
        adapter = MainAdapter(arrayListOf())
        root.rvMusic.addItemDecoration(
            DividerItemDecoration(
                root.rvMusic.context,
                (root.rvMusic.layoutManager as LinearLayoutManager).orientation
            )
        )
        root.rvMusic.adapter = adapter
    }
}