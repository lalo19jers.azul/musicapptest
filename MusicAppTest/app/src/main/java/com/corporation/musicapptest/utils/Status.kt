package com.corporation.musicapptest.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}