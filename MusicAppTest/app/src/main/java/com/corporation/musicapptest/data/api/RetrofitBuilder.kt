package com.corporation.musicapptest.data.api

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

import java.io.IOException

object RetrofitBuilder {
    private const val BASE_URL = "https://api.lyrics.ovh"

    private fun getRetrofit(): Retrofit{


        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    }

    val apiService: ApiService = getRetrofit().create(ApiService::class.java)
}


