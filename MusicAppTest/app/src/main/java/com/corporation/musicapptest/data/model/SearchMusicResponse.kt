package com.corporation.musicapptest.data.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class SearchMusicResponse {
    @SerializedName("data")
    @Expose
    private var data: List<Datum?>? = null
    @SerializedName("total")
    @Expose
    private var total: Int? = null
    @SerializedName("next")
    @Expose
    private var next: String? = null

    fun getData(): List<Datum?>? {
        return data
    }

    fun setData(data: List<Datum?>?) {
        this.data = data
    }

    fun getTotal(): Int? {
        return total
    }

    fun setTotal(total: Int?) {
        this.total = total
    }

    fun getNext(): String? {
        return next
    }

    fun setNext(next: String?) {
        this.next = next
    }

}