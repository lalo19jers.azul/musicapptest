package com.corporation.musicapptest.data.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class Album {
    @SerializedName("id")
    @Expose
    private var id: Int? = null
    @SerializedName("title")
    @Expose
    private var title: String? = null
    @SerializedName("cover")
    @Expose
    private var cover: String? = null
    @SerializedName("cover_small")
    @Expose
    private var coverSmall: String? = null
    @SerializedName("cover_medium")
    @Expose
    private var coverMedium: String? = null
    @SerializedName("cover_big")
    @Expose
    private var coverBig: String? = null
    @SerializedName("cover_xl")
    @Expose
    private var coverXl: String? = null
    @SerializedName("tracklist")
    @Expose
    private var tracklist: String? = null
    @SerializedName("type")
    @Expose
    private var type: String? = null

    fun getId(): Int? {
        return id
    }

    fun setId(id: Int?) {
        this.id = id
    }

    fun getTitle(): String? {
        return title
    }

    fun setTitle(title: String?) {
        this.title = title
    }

    fun getCover(): String? {
        return cover
    }

    fun setCover(cover: String?) {
        this.cover = cover
    }

    fun getCoverSmall(): String? {
        return coverSmall
    }

    fun setCoverSmall(coverSmall: String?) {
        this.coverSmall = coverSmall
    }

    fun getCoverMedium(): String? {
        return coverMedium
    }

    fun setCoverMedium(coverMedium: String?) {
        this.coverMedium = coverMedium
    }

    fun getCoverBig(): String? {
        return coverBig
    }

    fun setCoverBig(coverBig: String?) {
        this.coverBig = coverBig
    }

    fun getCoverXl(): String? {
        return coverXl
    }

    fun setCoverXl(coverXl: String?) {
        this.coverXl = coverXl
    }

    fun getTracklist(): String? {
        return tracklist
    }

    fun setTracklist(tracklist: String?) {
        this.tracklist = tracklist
    }

    fun getType(): String? {
        return type
    }

    fun setType(type: String?) {
        this.type = type
    }
}