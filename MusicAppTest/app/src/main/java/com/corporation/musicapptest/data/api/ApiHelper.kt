package com.corporation.musicapptest.data.api

class ApiHelper(private val apiService: ApiService, private val term: String) {
   // suspend fun getSuggest() = apiService.getSuggest(term)
     fun getSuggest() = apiService.doMethod(term)
}