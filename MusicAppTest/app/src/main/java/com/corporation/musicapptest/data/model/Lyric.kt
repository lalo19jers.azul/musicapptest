package com.corporation.musicapptest.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Lyric {
    @SerializedName("lyrics")
    @Expose
    var lyrics: String? = null

}