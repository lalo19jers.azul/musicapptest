package com.corporation.musicapptest.ui.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.corporation.musicapptest.data.api.ApiHelper
import com.corporation.musicapptest.data.repository.MainRepositoty
import com.corporation.musicapptest.ui.main.viewmodel.MainViewModel
import java.lang.IllegalArgumentException

class ViewModelFactory(private val apiHelper: ApiHelper): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)){
            return MainViewModel(MainRepositoty(apiHelper)) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }

}