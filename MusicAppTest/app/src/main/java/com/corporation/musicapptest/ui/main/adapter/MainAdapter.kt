package com.corporation.musicapptest.ui.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.corporation.musicapptest.R
import com.corporation.musicapptest.data.model.Datum
import kotlinx.android.synthetic.main.item_music.view.*
class MainAdapter(private val musicList: List<Datum>) :
    RecyclerView.Adapter<MainAdapter.ViewHolder>() {
    private var onItemClickListener: ItemClickListener? = null

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val v = LayoutInflater.from(p0?.context).inflate(R.layout.item_music, p0, false)
        return ViewHolder(v);
    }

    override fun getItemCount(): Int {
        return musicList.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.songName?.text = musicList[position].getTitleShort()
        viewHolder.artist?.text = musicList[position].getArtist()!!.getName()
        viewHolder.album?.text = musicList[position].getAlbum()!!.getTitle()
        Glide.with(viewHolder.imageSong.context)
            .load(musicList[position].getArtist()!!.getPictureXl())
            .into(viewHolder.imageSong)

        viewHolder.itemView.setOnClickListener {
            onItemClickListener?.onItemClick(viewHolder.itemView, position)
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val songName = itemView.findViewById<TextView>(R.id.tvSongName)
        val artist = itemView.findViewById<TextView>(R.id.tvArtist)
        val album = itemView.findViewById<TextView>(R.id.tvAlbum)
        val imageSong = itemView.findViewById<ImageView>(R.id.ivSong)
    }


    fun setItemClickListener(clickListener: ItemClickListener) {
        onItemClickListener = clickListener
    }

    interface ItemClickListener {
        fun onItemClick(view: View, position: Int)
    }
}